# variables
variable "company_network" {
  type = "string"
  default = "10.0.0.0/8"
}

variable "azure-east-us-vnet" {
  type = "string"
  default = "10.90.0.0/16"
}

variable "west-chester-net" {
  type = "string"
  default = "172.16.0.0/12"
}

variable "atlanta-net" {
  type = "string"
  default = "172.25.0.0/12"
}

